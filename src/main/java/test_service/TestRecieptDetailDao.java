/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.sirapob.databaseproject.dao.ProductDao;
import com.sirapob.databaseproject.dao.RecieptDao;
import com.sirapob.databaseproject.dao.RecieptDetailDao;
import com.sirapob.databaseproject.model.Product;
import com.sirapob.databaseproject.model.Reciept;
import com.sirapob.databaseproject.model.RecieptDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sirapob
 */
public class TestRecieptDetailDao {
    public static void main(String[] args) {
        RecieptDetailDao rdd = new RecieptDetailDao();
        for(RecieptDetail rd :rdd.getAll()){
            System.out.println(rd);
        }
        RecieptDao rd = new RecieptDao();
        ProductDao pd = new ProductDao();
        List<Product> products = pd.getAll();
        Product product0 = products.get(0);
        Reciept reciept = rd.get(1);
        RecieptDetail newRecieptDetail = new RecieptDetail(product0.getId(),product0.getName(),product0.getPrice(), 1, product0.getPrice()*1, reciept.getId());
        rdd.save(newRecieptDetail);
    }
}

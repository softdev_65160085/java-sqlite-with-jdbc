/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sirapob.databaseproject.component;

import com.sirapob.databaseproject.model.Product;

/**
 *
 * @author Sirapob
 */
public interface BuyProductable {
    public void buy(Product product,int qty);
}

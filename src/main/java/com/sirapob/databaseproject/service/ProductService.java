/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sirapob.databaseproject.service;

import com.sirapob.databaseproject.dao.ProductDao;
import com.sirapob.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author Sirapob
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName(){
        return (ArrayList<Product>) productDao.getAll(" product_name ASC ");

    }
}

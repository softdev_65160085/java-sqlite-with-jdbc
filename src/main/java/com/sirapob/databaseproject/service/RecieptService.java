/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sirapob.databaseproject.service;

import com.sirapob.databaseproject.dao.RecieptDao;
import com.sirapob.databaseproject.dao.RecieptDetailDao;
import com.sirapob.databaseproject.model.Reciept;
import com.sirapob.databaseproject.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author Sirapob
 */
public class RecieptService {
    public Reciept getById(int id){
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.get(id);
    }
    
    public List<Reciept> getReciepts(){
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" reciept_id asc");
    }

    public Reciept addNew(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept = recieptDao.save(editedReciept);
        for(RecieptDetail rd: editedReciept.getRecieptDetails()){
            rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return  reciept;
    }

    public Reciept update(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
